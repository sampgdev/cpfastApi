from typing import Optional
from pydantic import BaseModel, Field

class Movie(BaseModel):
    idmovie: Optional[int]= None
    title: str = Field(min_length=5, max_length=25)
    overview: str = Field(min_length=15, max_length=50)
    year: int = Field(le=2023)
    rating: float = Field(lt=10.0)
    category: str = Field(min_length=5, max_length=25)
    class Config:
       	schema_extra = {
             "example" : {
                  "idmovie": 1,
                  "title" : "mi peli",
                  "overview" : "descripcion de la peli",
                  'year': 2000,
                  "rating": 9.9,
                  "category": 'Accion'   
			 }
		}
