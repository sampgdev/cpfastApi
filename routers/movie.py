from fastapi import APIRouter, Depends,Path, Query
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from typing import List
from middelware.jwt_bearer import JWTBearer
from config.database import Session
from servicios.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()


@movie_router.get('/movies', tags=['movies'])
def get_movies():
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/movies/{id}', tags=['movies'])
def get_movie(id:int=Path(ge=1,le=100))->Movie:
    db = Session()
    result = MovieService(db).get_movie_id(id)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))
    
@movie_router.get('/movies/',  tags=['movies'], response_model= List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies_category(category: str = Query(min_length=5, max_length=50))->Movie:
    db = Session()
    result = MovieService(db).get_movie_category(category)
    if not result:
    	return JSONResponse(status_code=404, content={'message':'Recurso no encontrado'})    
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.post('/movies', tags=['movies'], response_model=dict, status_code=201)
def create_movie(movie:Movie):
    db = Session()
    MovieService(db).create_movie(movie)
    return JSONResponse(status_code=201, content={"message":"creado correctamente"})
	
@movie_router.put("/movies/{id}", tags=['movies'])
def update_movie(id:int, movie:Movie):
    db = Session()
    item = MovieService(db).get_movie_id(id)
    if not item:
        return JSONResponse(status_code=404, content={'message':'Recurso no encontrado'})
    
    MovieService(db).update_movie(id, movie)
    return JSONResponse(status_code=200, content={"message":"Actualizado correctamente"})

@movie_router.delete("/movies/{id}", tags=['movies'])
def delete_movie(id:int):
    db = Session()
    item = MovieService(db).delete_movie(id)
    if not item:
        return JSONResponse(status_code=404, content={'message':'Recurso no encontrado'})
    
    return JSONResponse(status_code=200, content={"message":"Eliminado correctamente"})